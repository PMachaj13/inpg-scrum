#include <utility>

#include <utility>

#include <utility>

#include <utility>

#ifndef INPG_SCRUM_INPG_SCRUM_HPP
#define INPG_SCRUM_INPG_SCRUM_HPP

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <cstdlib>

class Contact{
public:
    Contact()= default;
    Contact(std::string name ,std::string surname , unsigned int number) :
                                    name_(std::move(name)),surname_(std::move(surname)), number_(number) {toString();}
    void setName_(std::string name){Contact::name_ = std::move(name);}
    void setSurname_(std::string surname){Contact::surname_ = std::move(surname);}
    void setNumber_(unsigned int number){Contact::number_ = number;}
    std::string getName_()const{ return name_;}
    std::string getSurname_()const{ return surname_;}
    unsigned int getNumber_()const{ return number_;}
    std::string getSignature()const{return signature_;}
    void toString();
    void actualize();
private:
    std::string signature_;
    std::string name_;
    std::string surname_;
    unsigned int number_{};
};

class Book{
    std::vector<Contact*> book_;
public:
    Book()= default;
    explicit Book(std::vector<Contact*> book) : book_(std::move(book)) {Sort();}
    Contact* operator[](std::size_t pos) { return book_[pos]; }
    void actualize_contact_by_surname();
    void print_book() {Sort(); for(const auto& a : book_) std::cout<<a->getSignature();}
    void add_contact(Contact* con);
    void add_contact(std::string,std::string,unsigned int);
    void add_contact();
    void del_contact();
    void Sort(){std::size_t a=book_.size();sort(0, a-1);}
    void sort(std::size_t,std::size_t);
    std::size_t sort_transform(std::size_t,std::size_t);
    std::size_t sort_comparison(std::size_t,std::size_t);
    std::size_t sort_split_point(std::size_t a,std::size_t b){return a+(b-a)/2;}
    void swap(std::size_t a,std::size_t b);
    void find_contact();
    std::vector<Contact*> search (std::string);
    bool isApartB(std::string,std::string);
    std::size_t total(){return book_.size();}
    std::string to_string() const;
};
void write_to_txt (Book& b);
void read_from_txt (Book& b);
void meniu();


#endif //INPG_SCRUM_INPG_SCRUM_HPP