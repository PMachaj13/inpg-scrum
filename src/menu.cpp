#include "inpg_scrum.hpp"

void meniu() {
    using std::cout;
    Book book;
    read_from_txt(book);
    int wybor;
    do {

        cout << std::setw(40) << "---Ksiazka telefoniczna---\n\n";
        cout << "Mozliwe dzialania:\n";
        cout << "1.Dodawanie kontaktu.\n";
        cout << "2.Wyszukiwanie kontaktu.\n";
        cout << "3.Usuwanie kontaktu.\n";
        cout << "4.Wypisz wszystkie kontakty.\n";
        cout << "5.Aktualizowanie kontaktu.\n\n";
        cout << "0.Koniec. \n";
        cout << "\nPodaj wybor: ";

        std::cin >> wybor;
        if (wybor >= 0 && wybor <= 6) {
            switch (wybor) {
                case 1: {
                    book.add_contact();
                    break;
                }
                case 2: {
                    book.find_contact();
                    break;
                }
                case 3: {
                    cout << "Oto aktualna lista kontaktow:\n";
                    book.print_book();
                    book.del_contact();
                    break;
                }
                case 4: {
                    cout << "Spis wszystkich kontaktow:\n";
                    book.print_book();
                    system("PAUSE");
                    break;
                }
                case 5: {
                    book.actualize_contact_by_surname();
                    break;
                }
                default:break;
            }
        } else {
            cout << "Wybrano metode spoza listy!! Wybierz ponownie:\n";
        }
    } while (wybor != 0);
    cout << "Koniec.";
    write_to_txt(book);
}