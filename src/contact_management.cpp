#include <utility>

#include <utility>

#include "inpg_scrum.hpp"


void Contact::actualize() {
    std::size_t decision=0;
    std::cout<<"Aktualne dane:";
    std::cout<<getSignature();
    std::cout<<"Czy chcesz zmienic: 1. Wszystkie dane, 2. Imie, 3. Nazwisko, 4. Numer, 5.Wyjdz do menu";
    std::cin>>decision;
    switch (decision) {
        case 1: {
            std::string im , su;
            unsigned int num = 0;
            std::cout << "Nowe imie:";
            std::cin >> im;
            setName_(im);
            std::cout << "Nowe nazwisko:";
            std::cin >> su;
            setSurname_(su);
            std::cout << "Nowy numer:";
            std::cin >> num;
            setNumber_(num);
            toString();

            break;
        }
        case 2: {
            std::string im;
            std::cout << "Nowe imie:";
            std::cin >> im;
            setName_(im);
            toString();
            break;
        }
        case 3: {
            std::string su;
            std::cout << "Nowe nazwisko:";
            std::cin >> su;
            setSurname_(su);
            toString();
            break;
        }
        case 4: {
            unsigned int num = 0;
            std::cout << "Nowy numer:";
            std::cin >> num;
            setNumber_(num);
            toString();
            break;
        }
        case 5: {
            break;
        }
        default:
            break;
    }
}
void Book::add_contact() {
    auto * ptr = new Contact;
    unsigned int nr;
    std::string name;
    std::string sname;
    std::string group;
    std::cout<<"Podaj imie"<<std::endl;
    std::cin >> name;
    std::cout<<"Podaj nazwisko"<<std::endl;
    std::cin>> sname;
    std::cout<<"Podaj numer telefonu"<<std::endl;
    std::cin >> nr;

    ptr->setName_(name);
    ptr->setSurname_(sname);
    ptr->setNumber_(nr);
    ptr->toString();

    this->book_.push_back(ptr);
    Sort();
}
void Book::add_contact(Contact* con){
    book_.push_back(con);
    Sort();
}
void Book::add_contact(std::string name,std::string sname, unsigned int num){
    Contact * temptr=new Contact (std::move(name), std::move(sname),num);
    book_.push_back(temptr);
    Sort();
}
void Book::del_contact() {
    std::cout<<"Wybierz kontakt do usuniecia:"<<std::endl;
    std::string phrase;
    std::vector<std::size_t> list;
    std::cin>>phrase;
    std::string yn;
    if ((phrase[0]>='a'&&phrase[0]<='z')||(phrase[0]>='A'&&phrase[0]<='Z')){
        for (std::size_t i=0;i<book_.size();i++)
            if (isApartB(phrase,book_[i]->getSurname_())) {
                list.push_back(i);
                std::cout <<list.size()<<" . "<< book_[i]->getSignature();
            }
        if(list.size()==1){
            std::cout<<"Czy chcesz usunac ten kontakt?(y/n)"<<std::endl;
            std::cin>>yn;
            if(yn[0]=='y') {
                book_.erase(book_.begin() + list[0]);
                std::cout << "Kontakt zostal usuniety";
            }
        }
        else if(list.size()>1){
            std::cout<<"Czy chcesz usunac te kontakty (a),jeden z nich (o), czy zaden (n)"<<std::endl;
            std::cin>>yn;
            if(yn[0]=='a')
                for (std::size_t a=list.size();a>0;a--)
                    book_.erase(book_.begin()+list[a-1]);
            else if(yn[0]=='o'){
                std::cout<<"Ktory kontakt chcesz usunac?"<<std::endl;
                unsigned int chose;
                std::cin>>chose;
                if(chose>0&&chose<=list.size())
                    book_.erase(book_.begin()+ list[chose-1]);
                else std::cout<<"Bledny numer"<<std::endl;
            }
        }
        else if(list.empty()) std::cout<<"Brak wynikow :<<"<<std::endl;
        Sort();
    }
    else {
        std::cout << "blad wyszukiwania" << std::endl;;
    }

}

void Book::actualize_contact_by_surname(){
    print_book();
    std::cout<<"Wybierz kontakt do aktualizacji:"<<std::endl;
    std::string phrase;
    std::cin>>phrase;
    std::vector<Contact*>contacts_collection=search(phrase);
    if (contacts_collection.size()==0)
        std::cout<<"Blad wyszukiwania";
    else if (contacts_collection.size()==1){
        contacts_collection[0]->actualize();
    }
    else{
        for(std::size_t i = 1; i<=contacts_collection.size(); i++){
            std::cout<<i<<". "<<contacts_collection[i-1]->getSignature()<<std::endl;
        }
        std::size_t decision;
        std::cout<<"Ktory kontakt chcesz aktualizowac:";
        std::cin>>decision;
        contacts_collection[decision-1]->actualize();
        std::cout<<"Dane po aktualizacji:"<<std::endl;
        contacts_collection[decision-1]->getSignature();
    }
    Sort();
}