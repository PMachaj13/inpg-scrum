#include "inpg_scrum.hpp"
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iomanip>


void Contact::toString() {
    std::size_t i;
    std::string str;
    std::string temp=surname_;
    for(i=0;i<temp.size();i++)
        temp[i]=toupper(temp[i]);
    str+=temp;
    for(;i<20;i++)
        str+=" ";
    temp=name_;
    temp[0]=toupper(temp[0]);
    for(i=1;i<temp.size();i++)
        temp[i] = tolower(temp[i]);
    str+=temp;
    for(;i<15;i++)
        str+=" ";
    str+="  ";
    str+=std::to_string(number_);
    str+='\n';
    str+='\n';
    signature_=str;
}

