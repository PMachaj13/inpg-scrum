#include "inpg_scrum.hpp"

std::vector<Contact*> Book::search (std::string phrase){
    std::vector<Contact*> ret;
    if(phrase[0]>='0'&&phrase[0]<='9'){
        for (auto &i : book_) {
            if(isApartB(phrase,std::to_string(i->getNumber_()))){
                ret.push_back(i);
            }
        }
    }
    else if ((phrase[0]>='a'&&phrase[0]<='z')||(phrase[0]>='A'&&phrase[0]<='Z')){
        for (auto &i : book_)
            if (isApartB(phrase, i->getSurname_())) {
                ret.push_back(i);
            }

    }
    return ret;
}
void Book::find_contact(){
    std::string phrase;
    std::cout<<"Wpisz szukana fraze:"<<std::endl;
    std::cin>>phrase;
    std::vector<Contact*> results=search(phrase);
    if(results.empty())
        std::cout << "Brak wynikow" << std::endl;
    else for(auto& a : results)
        std::cout<<a->getSignature();
}
bool Book::isApartB(std::string a,std::string b){
    std::size_t a_lenght = a.size();
    bool retval=1;
    for (std::size_t i = 0; i<a_lenght; i++) {
        if(tolower(a[i])!=tolower(b[i])) retval=0;
    }
    return retval;
}