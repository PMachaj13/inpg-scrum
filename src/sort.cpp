#include "inpg_scrum.hpp"

void Book::sort(std::size_t left, std::size_t right) {
    if(right>left){
        std::size_t position=sort_transform(left,right);
        if(position>left) sort(left,position-1);
        if(position<right) sort(position+1,right);
    }
}

std::size_t Book::sort_comparison(std::size_t a,std::size_t b){
    size_t i = 0;
    for (; i < book_[a]->getSignature().size()&&book_[a]->getSignature()[i]==book_[b]->getSignature()[i]; ++i)
        ;
    if( i < book_[a]->getSignature().size()&&book_[a]->getSignature()[i]<=book_[b]->getSignature()[i]){
        return a;}
    return b;
}
std::size_t Book::sort_transform(std::size_t left,std::size_t right){
    std::size_t div_index=sort_split_point(left,right);
    swap(div_index,right);
    std::size_t cur_pos=left;
    for(std::size_t i=left;i<right;i++)
        if(i==sort_comparison(i,right)){
            swap(i,cur_pos);
            cur_pos++;
        }
    swap(cur_pos,right);
    return cur_pos;
}
void Book::swap(std::size_t a,std::size_t b){
    if(a!=b){
        Contact* temp=book_[a];
        book_[a]=book_[b];
        book_[b]=temp;
    }
}
