#include "inpg_scrum.hpp"
#include <sstream>
#include <fstream>

std::string Book::to_string() const{
    std::ostringstream oss;
    for(const auto& a : book_){
        oss<<a->getName_()<<","<<a->getSurname_()<<","<<a->getNumber_()<<","<<std::endl;
    }
    return oss.str();
}


void write_to_txt ( Book& b) {
    b.Sort();
    std::fstream plik;
    plik.open( "C:\\Users\\Lesiuu\\Desktop\\Nowy folder\\inpg-scrum\\contacts.txt", std::ios::in | std::ios::out);
    if( plik.good() )
    {
        plik<<b.to_string()<<'?';
    } else std::cout << "Error #404 not found" << std::endl;
}
void read_from_txt (Book& b) {
    unsigned int  nr_tel;
    std::size_t i=0;
    std::string linia;
    std::fstream plik;
    plik.open("C:\\Users\\Lesiuu\\Desktop\\Nowy folder\\inpg-scrum\\contacts.txt", std::ios::in);
    if (!plik.good())
    {
        std::cout << "Error #666 not found"<<std::endl;
        exit(0);
    }

    while (getline(plik, linia)&&linia[0]!='?') {
        std::string n,s,t;
        for(i=0;linia[i]!=',';i++)
            n+=linia[i];

        i++;
        for(;linia[i]!=',';i++)
            s+=linia[i];
        i++;
        for(;linia[i]!=',';i++)
            t+=linia[i];
        if(n[0]!=' '&&s[0]!=' '&&t[0]!=' ') {
            nr_tel = static_cast<unsigned int>(std::stoi(t));
            b.add_contact(n, s, nr_tel);
        }
    }
    plik.close();

}