cmake_minimum_required(VERSION 3.13)
project(inpg_scrum)

set(CMAKE_CXX_STANDARD 14)
add_compile_options(-Wall -Wextra -Werror -Wpedantic -pedantic-errors)

include_directories(
        include
)
set(SOURCE_FILES
        src/inpg_scrum.cpp
        src/sort.cpp
        src/search.cpp
        src/ioflow.cpp
        src/contact_management.cpp
        src/menu.cpp)

add_executable(inpg_scrum ${SOURCE_FILES} main.cpp)

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/contacts.txt
        ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
